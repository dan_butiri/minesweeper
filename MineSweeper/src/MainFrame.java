import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.SwingConstants;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.ButtonGroup;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


@SuppressWarnings("serial")
public class MainFrame extends JFrame 
{
	private GamePanel gamePanel = new GamePanel(UtilClass.LEVEL_X, UtilClass.LEVEL_Y, UtilClass.LEVEL_MINE);
	private JMenuBar menuBar = new JMenuBar();
	private JMenu mnGame = new JMenu("Game");
	private JMenu mnHelp = new JMenu("Help");
	private JMenuItem mntmAbout = new JMenuItem("     About     ");
	private JMenuItem mntmNew = new JMenuItem("New");
	private JCheckBoxMenuItem mntmBeginner = new JCheckBoxMenuItem("Beginner");
	private JCheckBoxMenuItem mntmIntermediate = new JCheckBoxMenuItem("Intermediate");
	private JCheckBoxMenuItem mntmExpert = new JCheckBoxMenuItem("Expert");
	private JCheckBoxMenuItem mntmCustom = new JCheckBoxMenuItem("Custom...");
	private JMenuItem mntmBestTimes = new JMenuItem("Best Times...");
	private JMenuItem mntmExit = new JMenuItem("Exit");
	private JMenuItem mntmHelp = new JMenuItem("     Help     ");
	private ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Constructor pt. clasa MainFrame.
	 * @param x Coordonata x din matrice (linia).
	 * @param y Coordonata y din matrice (coloana).
	 * @param mine mine NrMine nr. de mine.
	 */
	public MainFrame(int x, int y, int mine) 
	{
		setIconImage(Toolkit.getDefaultToolkit().getImage(Constants.WIN_PIC));
		setJMenuBar(menuBar);
		buttonGroup.add(mnGame);
		menuBar.add(mnGame);

		mntmNew.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0));
		
		mnGame.add(mntmNew);
		buttonGroup.add(mntmBeginner);
		mntmBeginner.setSelected(true);
		
		mnGame.addSeparator();
		
		mnGame.add(mntmBeginner);
		buttonGroup.add(mntmIntermediate);
		
		mnGame.add(mntmIntermediate);
		buttonGroup.add(mntmExpert);
		
		mnGame.add(mntmExpert);
		buttonGroup.add(mntmCustom);
		
		mnGame.add(mntmCustom);
		
		mnGame.addSeparator();
		mntmBestTimes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg) {
				String format = "Beginner:          " + UtilClass.BEGINNER_BEST_TIME_NAME + "\nIntermediate:   " + 
								UtilClass.INTERMEDIATE_MAX_TIME_NAME +
								"\nExpert:               " + UtilClass.EXPERT_MAX_TIME_NAME;
				JOptionPane.showMessageDialog(null, format, "Clasament", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		mnGame.add(mntmBestTimes);
		
		mnGame.addSeparator();
		mntmExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0));
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg) {
				System.exit(0);
			}
		});
		
		mnGame.add(mntmExit);
		
		menuBar.add(mnHelp);
		mntmHelp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg) {
				JOptionPane.showMessageDialog(null,Constants.RULES,"Minesweeper: reguli si notiuni de baza",JOptionPane.INFORMATION_MESSAGE);
			}
		});

		mntmHelp.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
		
		mnHelp.add(mntmHelp);

		mntmAbout.setHorizontalAlignment(SwingConstants.RIGHT);
		
		mnHelp.addSeparator();
		
		mnHelp.add(mntmAbout);
		mntmAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg) {
				JOptionPane.showMessageDialog(null,Constants.ABOUT,"Date personale si scop",JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		setContentPane(gamePanel);
		
		this.setVisible(true);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Minesweeper");
		this.pack();
	}
	
	/**
	 * Getter pt. gamePanel
	 * @return gamePanel Panel`ul principal.
	 */
	public GamePanel getGamePanel()
	{
		return gamePanel;
	}
	
	/**
	 * Metoda pt. adaugarea unui ActionListener pe JMenuItem'ul de joc nou.
	 * @param al ActionListener.
	 */
	public void addmntmNewActionListener(ActionListener al)
	{
		mntmNew.addActionListener(al);
	}
	
	/**
	 * Metoda pt. adaugarea unui ActionListener pe JCheckBoxMenuItem'ul pt. versiuna Beginner.
	 * @param al ActionListener.
	 */
	public void addmntmBeginnerActionListener(ActionListener al)
	{
		mntmBeginner.addActionListener(al);
	}
	
	/**
	 * Metoda pt. adaugarea unui ActionListener pe JCheckBoxMenuItem'ul pt. versiuna Intermediate.
	 * @param al ActionListener.
	 */
	public void addmntmIntermediateActionListener(ActionListener al)
	{
		mntmIntermediate.addActionListener(al);
	}
	
	/**
	 * Metoda pt. adaugarea unui ActionListener pe JCheckBoxMenuItem'ul pt. versiuna Expert.
	 * @param al ActionListener.
	 */
	public void addmntmExpertActionListener(ActionListener al)
	{
		mntmExpert.addActionListener(al);
	}
	
	/**
	 * Metoda pt. adaugarea unui ActionListener pe JCheckBoxMenuItem'ul pt. versiuna Custom.
	 * @param al ActionListener.
	 */
	public void addmntmCustomActionListener(ActionListener al)
	{
		mntmCustom.addActionListener(al);
	}
	
	/**
	 * Getter pt. mntmBeginner (buton din meniu pt. versiuna Beginner).
	 * @return mntmBeginner.
	 */
	public JCheckBoxMenuItem getmntmBeginner()
	{
		return mntmBeginner;
	}
	
	/**
	 * Getter pt. mntmIntermediate (buton din meniu pt. versiuna Intermediate).
	 * @return mntmIntermediate.
	 */
	public JCheckBoxMenuItem getmntmIntermediate()
	{
		return mntmIntermediate;
	}

	/**
	 * Getter pt. mntmExpert (buton din meniu pt. versiuna Expert).
	 * @return mntmExpert.
	 */
	public JCheckBoxMenuItem getmntmExpert()
	{
		return mntmExpert;
	}
	
	/**
	 * Getter pt. mntmCustom (buton din meniu pt. versiuna Custom).
	 * @return mntmCustom
	 */
	public JCheckBoxMenuItem getmntmCustom()
	{
		return mntmCustom;
	}
}
