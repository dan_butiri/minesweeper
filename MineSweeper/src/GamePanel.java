import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.border.BevelBorder;


@SuppressWarnings("serial")
public class GamePanel extends JPanel 
{
	private InfoPanel infoPanel;
	private TablePanel tablePanel;
	
	/**
	 * Constructor pt. clasa GamePanel.
	 * @param x Coordonata x din matrice (linia).
	 * @param y Coordonata y din matrice (coloana).
	 * @param mine NrMine nr. de mine.
	 */
	public GamePanel(int x, int y, int mine) 
	{
		init(x, y, mine);
	}
	
	/**
	 * Metoda seteaza proprietatile si adauga elemente pe panel'ului GamePanel.
	 * @param x Coordonata x din matrice (linia).
	 * @param y Coordonata y din matrice (coloana).
	 * @param mine NrMine nr. de mine.
	 */
	public void init(int x, int y, int mine)
	{
		infoPanel = new InfoPanel(mine);
		tablePanel = new TablePanel(x,y);
		this.add(infoPanel);
		this.add(tablePanel);
		this.setVisible(true);
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		this.setBorder(new BevelBorder(BevelBorder.RAISED));
	}
	
	/**
	 * Getter pt. panel`ul cu matricea de butonane.
	 * @return tablePanel - Panel`ul cu matricea de butoane.
	 */
	public TablePanel getTablePanel()
	{
		return tablePanel;
	}
	
	/**
	 * Getter pt. panel`ul cu informatii (numarul de mine, starea jocului, timpul).
	 * @return infoPanel - Panel`ul cu informati despre starea jocului.
	 */
	public InfoPanel getInfoPanel()
	{
		return infoPanel;
	}
}
