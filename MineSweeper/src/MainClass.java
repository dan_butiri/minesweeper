/**
 * 
 * @author Butiri Alexandru Dan
 *
 */
public class MainClass 
{
	/**
	 * Metoda main.
	 * @param args
	 */
	public static void main(String[] args) 
	{
		@SuppressWarnings("unused")
		ControlClass controlClass =  new ControlClass();
	}
}
