import java.util.Random;

public class AlgorithmClass
{
	private Random rn = new Random();
	private int [][]table;
	
	/**
	 * Constructor pt. Clasa Algorithm.
	 * @param x Nr. de linii.
	 * @param y Nr. de coloane.
	 * @param NrMine nr. de mine.
	 */
	public AlgorithmClass(int x, int y, int nrMine)
	{
		init(x, y, nrMine);
	}
	
	/**
	 * Metoda initializeaza matricea suport.
	 * @param x Nr. de linii.
	 * @param x Nr. de linii.
	 * @param y Nr. de coloane.
	 * @param NrMine nr. de mine.
	 */
	public void init(int x, int y, int nrMine)
	{
		int rx,ry;
		
		table = new int[x][y];
		
		for(int i=0; i<nrMine; ++i)
		{
			
			do{
				rx = rn.nextInt(x);
				ry = rn.nextInt(y);
			}while(table[rx][ry] == Constants.MINE);
			
			table[rx][ry] = Constants.MINE;
		}
		borderMine();
	}
	
	/**
	 * Metoda cauta fiecare mina din matrice si incrementeaza fiecare element din jurul ei.
	 */
	private void borderMine()
	{
		int x, y;
		for(int i=0; i<table.length; ++i)
		{
			for(int j=0; j<table[0].length; ++j)
			{
				if(table[i][j] == Constants.MINE)
				{
					for(int k=0; k<Constants.X.length; ++k)
					{
						x = i + Constants.X[k];
						y = j + Constants.Y[k];
							
						if(isInTable(x,y) && table[x][y] != Constants.MINE)
						{
							++table[x][y];
						}
					}
				}
			}
		}
	}
	
	/**
	 * Metoda verifica daca coordonatele noului element sunt valide.
	 * @param x Coordonata x din matrice (linia).
	 * @param y Coordonata y din matrice (coloana).
	 * @return True - daca elementul se afla in matrice, false - daca elementul este in afara matricei.
	 */
	private boolean isInTable(int x, int y)
	{
		if(x >= 0 && x < table.length && y >= 0 && y < table[0].length)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Getter pt. un element anume din matrice table.
	 * @param x Coordonata x din matrice (linia).
	 * @param y Coordonata y din matrice (coloana).
	 * @return table[x][y] Un element anume din matrice table.
	 */
	public int getTableElement(int x, int y)
	{
		return table[x][y];
	}
	
	/**
	 * Setter pt. un element anume din matricea table.
	 * @param x Coordonata x din matrice (linia).
	 * @param y Coordonata y din matrice (coloana).
	 * @param val Valoarea care se va stoca in matrice.
	 */
	public void setTableElement(int x, int y, int val)
	{
		table[x][y] = val;
	}
	
}
