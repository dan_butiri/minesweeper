import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;


@SuppressWarnings("serial")
public class TablePanel extends JPanel 
{
	private JLabel [][]lblTable;

	/**
	 * Constructor pt. clasa TablePanel.
	 * @param x Coordonata x din matrice (linia).
	 * @param y Coordonata y din matrice (coloana).
	 */
	public TablePanel(int x, int y) 
	{
		lblTable = new JLabel[x][y];
		
		this.setLayout(new GridLayout(x, y));
		this.setBorder(new BevelBorder(BevelBorder.LOWERED));
		this.setForeground(Color.GRAY);
		this.setVisible(true);
		
		for(int i = 0; i < x; ++i)
		{
			for(int j = 0; j < y; ++j)
			{
				lblTable[i][j] = new JLabel("    ");
				this.add(lblTable[i][j]);
				lblTable[i][j].setFont(new Font("Cambria Math", Font.BOLD, 14));
				lblTable[i][j].setHorizontalAlignment(SwingConstants.CENTER);
				lblTable[i][j].setBorder(new BevelBorder(BevelBorder.RAISED));
				lblTable[i][j].setOpaque(true);
			}
		}
	}
	
	/**
	 * @param x Coordonata x din matrice (linia).
	 * @param y Coordonata y din matrice (coloana).
	 * @param ma MouseAdapter
	 */
	public void addlblTableMouseListener(int x, int y, MouseAdapter ma)
	{
		lblTable[x][y].addMouseListener(ma);
	}
	
	/**
	 * Getter pt. un element anume din matricea lblTable.
	 * @param x Coordonata x din matrice (linia).
	 * @param y Coordonata y din matrice (coloana).
	 * @return lblTable[x][y] Un element anume din matricea lblTable.
	 */
	public JLabel getlblTableElement(int x, int y)
	{
		return lblTable[x][y];
	}
	
	/**
	 * Getter pt. nr. de linii din lblTable.
	 * @return lblTable.length Nr. de linii din lblTable.
	 */
	public int getlblTableLengthX()
	{
		return lblTable.length;
	}
	
	/**
	 * Getter pt. nr. de coloane din lblTable.
	 * @return lblTable[0].length Nr. de coloane din lblTable.
	 */
	public int getlblTableLengthY()
	{
		return lblTable[0].length;
	}

}
