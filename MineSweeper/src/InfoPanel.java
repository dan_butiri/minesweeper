import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.ImageIcon;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;

import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.SwingConstants;


@SuppressWarnings("serial")
public class InfoPanel extends JPanel 
{
	private JTextField tfMine = new JTextField("");
	private JLabel lblReset = new JLabel();
	private JTextField tfTime = new JTextField("000");
	
	/**
	 * Constructor pt. clasa GamePanel.
	 * @param nrMine Numarul de mine.
	 */
	public InfoPanel(int nrMine) 
	{
		setLayout(new GridLayout(1, 3));
		
		tfMine.setHorizontalAlignment(SwingConstants.CENTER);
		tfMine.setBackground(Color.BLACK);
		tfMine.setText("" + nrMine);
		tfMine.setFont(new Font("Tahoma", Font.BOLD, 20));
		tfMine.setForeground(Color.RED);
		tfMine.setEditable(false);
		tfMine.setSelectionColor(Color.BLACK);
		tfMine.setSelectedTextColor(Color.RED);
		tfMine.setBorder(new BevelBorder(BevelBorder.RAISED));
		add(tfMine);
		
		lblReset.setIcon(new ImageIcon(Constants.SMILE_PIC));
		lblReset.setHorizontalAlignment(SwingConstants.CENTER);
		lblReset.setBorder(new BevelBorder(BevelBorder.RAISED));
		add(lblReset);
		
		tfTime.setHorizontalAlignment(SwingConstants.CENTER);
		tfTime.setBackground(Color.BLACK);
		tfTime.setFont(new Font("Tahoma", Font.BOLD, 20));
		tfTime.setForeground(Color.RED);
		tfTime.setEditable(false);
		tfTime.setSelectionColor(Color.BLACK);
		tfTime.setSelectedTextColor(Color.RED);
		tfTime.setBorder(new BevelBorder(BevelBorder.LOWERED));
		add(tfTime);
		
		this.setBorder(new BevelBorder(BevelBorder.LOWERED));
		this.setVisible(true);
	}
	
	/**
	 * Getter pt. TextField'ul in care este afisat nr. de mine.
	 * @return tfMine TextField'ul in care este afisat nr.de mine.
	 */
	public JTextField gettfMine()
	{
		return tfMine;
	}
	
	/**
	 * Getter pt. Label'ul de stare a jocului si reset.
	 * @return lblReset Label'ul in care este afisata stare a jocului.
	 */
	public JLabel getlblReset()
	{
		return lblReset;
	}
	
	/**
	 * Getter pt. TextField'ul in care este afisat timpul.
	 * @return tfTime TextField'ul in care este afisat timpul.
	 */
	public JTextField gettfTime()
	{
		return tfTime;
	}
	
	/**
	 * Metoda pt. adaugarea unui MouseListener pe JLabel'ul de restart.
	 * @param ma MouseAdapter
	 */
	public void addlblResetMouseListener(MouseAdapter ma)
	{
		lblReset.addMouseListener(ma);
	}
}
