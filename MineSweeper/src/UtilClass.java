import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.PrintStream;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JLabel;


public class UtilClass {
	public static int LEVEL_MINE = 10;
	public static int LEVEL_X = 9;
	public static int LEVEL_Y = 9;
	public static boolean isBEGINNER = true;
	public static boolean isINTERMEDIATE = false;
	public static boolean isEXPERT = false;
	public static boolean isCUSTOM = false;
	public static String TIME = "0";
	public static String BEGINNER_BEST_TIME_NAME = "999 Anonymous";
	public static String INTERMEDIATE_MAX_TIME_NAME = "999 Anonymous";
	public static String EXPERT_MAX_TIME_NAME = "999 Anonymous";
	
	/**
	 * Metoda pt. citirea din fisier a clasamentului.
	 */
	public void citireFisier(){
		try{
			FileReader fIn = new FileReader("Score.txt");
			BufferedReader read = new BufferedReader(fIn);
			
			BEGINNER_BEST_TIME_NAME = read.readLine();
			INTERMEDIATE_MAX_TIME_NAME = read.readLine();
			EXPERT_MAX_TIME_NAME = read.readLine();
			
			fIn.close();
		}catch(IOException ex){
			
		}
	}
	
	/**
	 * Metoda de scriere in fisier a clasamentului.
	 */
	public void scriereFisier(){
		try{
			FileOutputStream  fOut = new FileOutputStream ("Score.txt");
			PrintStream  writ = new PrintStream (fOut);
			
			writ.println(BEGINNER_BEST_TIME_NAME);
			writ.println(INTERMEDIATE_MAX_TIME_NAME);
			writ.println(EXPERT_MAX_TIME_NAME);
			
			fOut.close();
		}catch(IOException ex){
			
		}
	}
	
	/**
	 * Setter pt. setarea timpului si numelui jucatorului.
	 * @param score Timpul in care jocul a fost terminat.
	 * @param nume Numele jucatorului care a terminat jocul.
	 */
	public void setScore(String score ,String nume){
		if(isBEGINNER){
			BEGINNER_BEST_TIME_NAME = score + " " + nume;
		}
		if(isINTERMEDIATE){
			INTERMEDIATE_MAX_TIME_NAME = score + " " + nume;
		}
		if(isEXPERT){
			EXPERT_MAX_TIME_NAME = score + " " + nume;
		}
	}
	
	/**
	 * Bifeaza modul de joc (BEGINNER ,INTERMEDIATE ,EXPERT ,CUSTOM)
	 * @param b MenuItem BEGINNER.
	 * @param i MenuItem INTERMEDIATE.
	 * @param e MenuItem EXPERT.
	 * @param c MenuItem CUSTOM.
	 */
	public void setVersiune(JCheckBoxMenuItem b, JCheckBoxMenuItem i, JCheckBoxMenuItem e, JCheckBoxMenuItem c){
		if(isBEGINNER){
			b.setSelected(true);
		}
		if(isINTERMEDIATE){
			i.setSelected(true);
		}
		if(isEXPERT){
			e.setSelected(true);
		}
		if(isCUSTOM){
			c.setSelected(true);
		}
	}
	
	/**
	 * Debifeaza modul de joc (BEGINNER ,INTERMEDIATE ,EXPERT ,CUSTOM)
	 */
	public void setVersiuneFalse(){
		isBEGINNER = false;
		isINTERMEDIATE = false;
		isEXPERT = false;
		isCUSTOM = false;
	}
	
	/**
	 * Metoda pt. calcularea coordonatei x (linia).
	 * @param l JLabel in care sa actva Listener.
	 * @return ((l.getY() - 2) / l.getHeight()) Linia pe care se afla l in matrice.
	 */
	public int getX(JLabel l)
	{
		return ((l.getY() - 2) / l.getHeight());
	}

	/**
	 * Metoda pt. calcularea coordonatei y (coloana).
	 * @param l JLabel in care sa actva Listener.
	 * @return ((l.getX() - 2) / l.getWidth()) Coloana pe care se afla l in matrice.
	 */
	public int getY(JLabel l)
	{
		return ((l.getX() - 2) / l.getWidth());
	}
}
