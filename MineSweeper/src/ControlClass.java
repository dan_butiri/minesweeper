import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;


public class ControlClass 
{
	private MainFrame mainFrame = new MainFrame(UtilClass.LEVEL_X, UtilClass.LEVEL_Y, UtilClass.LEVEL_MINE);
	private ChronometerClass chronometerClass = new ChronometerClass(1, 999, mainFrame.getGamePanel().getInfoPanel().gettfTime());
	private AlgorithmClass algorithmClass = new AlgorithmClass(UtilClass.LEVEL_X, UtilClass.LEVEL_Y, UtilClass.LEVEL_MINE);
	private UtilClass  utilClass = new UtilClass();
	private FlagClass flagClass = new FlagClass();
	private JLabel lblAuxiliar;
	private boolean isMousePress = false;
	
	/**
	 * Constructor pt. Clasa Control.
	 */
	public ControlClass()
	{
		init();
	}
	
	/**
	 * Metoda initializeaza matricea suport.
	 */
	public void init()
	{
		utilClass.citireFisier();
		
		mainFrame.addmntmNewActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg)
			{
				newGame();
			}
		});
		
		mainFrame.addmntmBeginnerActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg)
			{
				
				UtilClass.LEVEL_X = 9;
				UtilClass.LEVEL_Y = 9;
				UtilClass.LEVEL_MINE = 10;
				utilClass.setVersiuneFalse();
				UtilClass.isBEGINNER = true;
				newGame();
			}
		});
		
		mainFrame.addmntmIntermediateActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg)
			{
				UtilClass.LEVEL_X = 16;
				UtilClass.LEVEL_Y = 16;
				UtilClass.LEVEL_MINE = 40;
				utilClass.setVersiuneFalse();
				UtilClass.isINTERMEDIATE = true;
				newGame();
			}
		});
		
		mainFrame.addmntmExpertActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg)
			{
				UtilClass.LEVEL_X = 16;
				UtilClass.LEVEL_Y = 30;
				UtilClass.LEVEL_MINE = 99;
				utilClass.setVersiuneFalse();
				UtilClass.isEXPERT = true;
				newGame();
			}
		});
		
		mainFrame.addmntmCustomActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg)
			{
				CustomPanel customPanel = new CustomPanel();
				int h = 9, w = 9, m = 10;
				
				customPanel.getTfHeight().setText("" + UtilClass.LEVEL_X);
				customPanel.getTfWidth().setText("" + UtilClass.LEVEL_Y);
				customPanel.getTfMines().setText("" + UtilClass.LEVEL_MINE);
				
				JOptionPane.showMessageDialog(null, customPanel ,"Custom Field", 3);
				try{
				h = Integer.parseInt(customPanel.getTfHeight().getText());
				w = Integer.parseInt(customPanel.getTfWidth().getText());
				m = Integer.parseInt(customPanel.getTfMines().getText());
				}catch(Exception ex){}
				if(h < 9) h = 9;
				if(h > 24) h = 24;
				if(w < 9) w = 9;
				if(w > 30) w = 30;
				if(m < 10) m = 10;
				if(m > 667) m = 667;
				UtilClass.LEVEL_X = h;
				UtilClass.LEVEL_Y = w;
				UtilClass.LEVEL_MINE = m;
				utilClass.setVersiuneFalse();
				UtilClass.isCUSTOM = true;
				newGame();
			}
		});
		
		mainFrame.getGamePanel().getInfoPanel().addlblResetMouseListener(new MouseAdapter()
		{
			public void mousePressed(MouseEvent arg) {
				mainFrame.getGamePanel().getInfoPanel().getlblReset().setBorder(new BevelBorder(BevelBorder.LOWERED));
			}
			public void mouseReleased(MouseEvent arg)
			{
				newGame();
			}
		});
		
		for(int i = 0; i < UtilClass.LEVEL_X; ++i)
		{
			for(int j = 0; j < UtilClass.LEVEL_Y; ++j)
			{
					mainFrame.getGamePanel().getTablePanel().addlblTableMouseListener(i, j, new MouseAdapter() 
					{	
						int x, y;
						@Override
						public void mouseEntered(MouseEvent arg) {
							
							lblAuxiliar = ((JLabel) arg.getComponent());
							x = utilClass.getX(((JLabel) arg.getComponent()));
							y = utilClass.getY(((JLabel) arg.getComponent()));
							
							if(algorithmClass.getTableElement(x, y) != Constants.NOT_ENABLE && isMousePress && !flagClass.isFlag(x, y)) 
							{
								((JLabel) arg.getComponent()).setBorder(new LineBorder(Color.DARK_GRAY, 1));;
							}
						}
						@Override
						public void mouseExited(MouseEvent arg) {
							x = utilClass.getX(((JLabel) arg.getComponent()));
							y = utilClass.getY(((JLabel) arg.getComponent()));
							
							if(algorithmClass.getTableElement(x, y) != Constants.NOT_ENABLE && isMousePress && !flagClass.isFlag(x, y)) 
							{
								((JLabel) arg.getComponent()).setBorder(new BevelBorder(BevelBorder.RAISED));
							}
						}
						@Override
						public void mousePressed(MouseEvent arg) {
							
							lblAuxiliar = ((JLabel) arg.getComponent());
							
							x = utilClass.getX(((JLabel) arg.getComponent()));
							y = utilClass.getY(((JLabel) arg.getComponent()));
							
							if(arg.getButton() == MouseEvent.BUTTON3 && algorithmClass.getTableElement(x, y) != Constants.NOT_ENABLE){
								
								int mine = Integer.parseInt(mainFrame.getGamePanel().getInfoPanel().gettfMine().getText());
								
								if(flagClass.isFlag(x, y)){
									flagClass.removeFlag(x, y);
									lblAuxiliar.setIcon(null);
									mainFrame.getGamePanel().getInfoPanel().gettfMine().setText("" + (++mine));
								}
								else{
									flagClass.addFlag(x, y);
									lblAuxiliar.setText("");
									lblAuxiliar.setIcon(new ImageIcon(Constants.FLAG_PIC));
									mainFrame.getGamePanel().getInfoPanel().gettfMine().setText("" + (--mine));
								}
							}
							
							if(algorithmClass.getTableElement(x, y) != Constants.NOT_ENABLE &&
							   arg.getButton() == MouseEvent.BUTTON1 && !flagClass.isFlag(x, y)) 
							{
								isMousePress = true;
								((JLabel) arg.getComponent()).setBorder(new LineBorder(Color.DARK_GRAY, 1));
							}
							
							mainFrame.getGamePanel().getInfoPanel().getlblReset().setIcon(new ImageIcon(Constants.OMG_PIC));
						}
						@Override
						public void mouseReleased(MouseEvent arg) 
						{
							x = utilClass.getX(((JLabel) arg.getComponent()));
							y = utilClass.getY(((JLabel) arg.getComponent()));
							
							mainFrame.getGamePanel().getInfoPanel().getlblReset().setIcon(new ImageIcon(Constants.SMILE_PIC));
							
							if(algorithmClass.getTableElement(x, y) != Constants.NOT_ENABLE && isMousePress && arg.getButton() == MouseEvent.BUTTON1)
							{	
								if(chronometerClass.isEnable())
								{
									chronometerClass.start();
								}
								
								x = utilClass.getX(lblAuxiliar);
								y = utilClass.getY(lblAuxiliar);
								
								if(!flagClass.isFlag(x, y)){
									verifica(lblAuxiliar);
								}
							}
							
						}
					});
			}
		}
	}
	
	/**
	 * Metoda pt. joc nou unde se reinitializeaza cu valoare default toate campurile si variablilele.
	 */
	private void newGame()
	{
		int xx, yy;
		mainFrame.dispose();
		chronometerClass.stop();
		xx = mainFrame.getX();
		yy = mainFrame.getY();
		mainFrame = new MainFrame(UtilClass.LEVEL_X, UtilClass.LEVEL_Y, UtilClass.LEVEL_MINE);
		mainFrame.setLocation(xx, yy);
		chronometerClass = new ChronometerClass(1, 999, mainFrame.getGamePanel().getInfoPanel().gettfTime());
		algorithmClass = new AlgorithmClass(UtilClass.LEVEL_X, UtilClass.LEVEL_Y, UtilClass.LEVEL_MINE);
		flagClass = new FlagClass();
		isMousePress = false;
		mainFrame.getGamePanel().getInfoPanel().gettfMine().setText("" + UtilClass.LEVEL_MINE);
		utilClass.setVersiune(mainFrame.getmntmBeginner(), mainFrame.getmntmIntermediate(),
				  mainFrame.getmntmExpert(), mainFrame.getmntmCustom());
		init();
	}
	
	/**
	 * Metoda care verifica daca jocul a fost castigat.
	 * @return True - daca jocul a fost castigat, false - daca jocul inca nu este gata.
	 */
	private boolean isWin()
	{
		int nr = 0;
		
		for(int i=0; i<getTablePanel().getlblTableLengthX(); ++i)
		{
			for(int j=0; j<getTablePanel().getlblTableLengthY(); ++j)
			{
				if(algorithmClass.getTableElement(i, j) == Constants.NOT_ENABLE || algorithmClass.getTableElement(i, j) == Constants.MINE)
				{
					++nr;
				}
			}
		}
		if(getTablePanel().getlblTableLengthX()*getTablePanel().getlblTableLengthY() == nr) return  true;
		else return false;
	}
	
	/**
	 * Metoda care opreste jocul deoarece a fost pierdut.
	 */
	private void gameOver()
	{
		chronometerClass.stop();
		
		mainFrame.getGamePanel().getInfoPanel().getlblReset().setIcon(new ImageIcon(Constants.SAD_PIC));
		
		for(int i = 0; i<getTablePanel().getlblTableLengthX(); ++i)
		{
			for(int j = 0; j<getTablePanel().getlblTableLengthY(); ++j)
			{
				if(algorithmClass.getTableElement(i, j) == Constants.MINE && flagClass.isFlag(i, j)){
					algorithmClass.setTableElement(i, j, Constants.NOT_ENABLE);
				}else
				if(algorithmClass.getTableElement(i, j) == Constants.MINE && !flagClass.isFlag(i, j) &&
				   algorithmClass.getTableElement(i, j) != Constants.NOT_ENABLE)
				{
					getTablePanel().getlblTableElement(i, j).setBorder(new LineBorder(Color.DARK_GRAY, 1));
					getTablePanel().getlblTableElement(i, j).setText("");
					getTablePanel().getlblTableElement(i, j).setBackground(Color.GRAY);
					getTablePanel().getlblTableElement(i, j).setIcon(new ImageIcon(Constants.MINE_PIC));
				}else
				if(flagClass.isFlag(i, j)){
					getTablePanel().getlblTableElement(i, j).setBorder(new LineBorder(Color.DARK_GRAY, 1));
					getTablePanel().getlblTableElement(i, j).setIcon(new ImageIcon(Constants.NOT_MINE_PIC));
					getTablePanel().getlblTableElement(i, j).setBackground(Color.GRAY);
				}
				
				algorithmClass.setTableElement(i, j, Constants.NOT_ENABLE);
			}
		}
	}
	
	/**
	 * Metoda care opreste jocul deoarece a fost castigat.
	 */
	private void gameWin()
	{
		chronometerClass.stop();
		
		for(int i = 0; i<getTablePanel().getlblTableLengthX(); ++i)
		{
			for(int j = 0; j<getTablePanel().getlblTableLengthY(); ++j)
			{	
				if(algorithmClass.getTableElement(i, j) == Constants.MINE)
				{
					getTablePanel().getlblTableElement(i, j).setText("");
					getTablePanel().getlblTableElement(i, j).setIcon(new ImageIcon(Constants.FLAG_PIC));
					algorithmClass.setTableElement(i, j, Constants.NOT_ENABLE);
				}
			}
		}
		
		mainFrame.getGamePanel().getInfoPanel().getlblReset().setIcon(new ImageIcon(Constants.WIN_PIC));
		
		utilClass.setScore(UtilClass.TIME, JOptionPane.showInputDialog("Nume: "));
		utilClass.scriereFisier();
	}
	
	/**
	 * Metoda care descopera butoanele in caz ca nu au nici o informatie pe ele pana la primul rand cu informatie.
	 * @param xx Coordonata x din matrice (linia).
	 * @param yy Coordonata y din matrice (coloana).
	 */
	private void discoverTable(int xx, int yy)
	{
		int x, y;
		
		getTablePanel().getlblTableElement(xx, yy).setBorder(new LineBorder(Color.DARK_GRAY, 1));
		getTablePanel().getlblTableElement(xx, yy).setBackground(Color.GRAY);
		algorithmClass.setTableElement(xx, yy, Constants.NOT_ENABLE);
		
		for(int i=0; i<Constants.X.length; i++)
		{
			x = xx + Constants.X[i];
			y = yy + Constants.Y[i];
			
			if(isInTable(x,y) && algorithmClass.getTableElement(x, y) == Constants.EMPTY_FIELD && 
					algorithmClass.getTableElement(x, y) != Constants.NOT_ENABLE && !flagClass.isFlag(x, y))
			{
				discoverTable(x, y);
			}else if(isInTable(x,y) && !flagClass.isFlag(x, y) && algorithmClass.getTableElement(x, y) != Constants.NOT_ENABLE){
				getTablePanel().getlblTableElement(x, y).setBorder(new LineBorder(Color.DARK_GRAY, 1));
				getTablePanel().getlblTableElement(x, y).setBackground(Color.GRAY);
				getTablePanel().getlblTableElement(x, y).setForeground(Constants.Colors[algorithmClass.getTableElement(x, y)]);
				getTablePanel().getlblTableElement(x, y).setText("" + algorithmClass.getTableElement(x, y));
				algorithmClass.setTableElement(x, y, Constants.NOT_ENABLE);
			}
		}
	}
	
	/**
	 * Metoda care verifica ce contine JLabel`ul apasat (mina, cifra sau nici o informatie).
	 * @param aux JLabel'ul care se doreste a se verifica.
	 */
	private void verifica(JLabel aux)
	{
		int x, y;
		
		x = utilClass.getX(aux);
		y = utilClass.getY(aux);
		
		aux.setBorder(new LineBorder(Color.DARK_GRAY, 1));
		isMousePress = false;
		switch(algorithmClass.getTableElement(utilClass.getX(aux),utilClass.getY(aux)))
		{
		case Constants.MINE :
			aux.setText("");
			aux.setIcon(new ImageIcon(Constants.RED_MINE_PIC));
			aux.setBackground(Color.RED);
			algorithmClass.setTableElement(x, y, Constants.NOT_ENABLE);
			gameOver();
			return;
		case Constants.EMPTY_FIELD :
			discoverTable(x, y);
			break;
		default:
			aux.setForeground(Constants.Colors[algorithmClass.getTableElement(utilClass.getX(aux), utilClass.getY(aux))]);
			aux.setBackground(Color.GRAY);
			aux.setText("" + algorithmClass.getTableElement(utilClass.getX(aux), utilClass.getY(aux)));
			break;
		}
		algorithmClass.setTableElement(x, y, Constants.NOT_ENABLE);
		if(isWin()) gameWin();
	}
	
	/**
	 * Metoda care verifica daca elementul accesat se afla in matrice.
	 * @param x Coordonata x din matrice (linia).
	 * @param y Coordonata y din matrice (coloana).
	 * @return True - daca se afla in matrice, false - daca nu se afla in matrice.
	 */
	private boolean isInTable(int x, int y)
	{
		if(x >= 0 && x < getTablePanel().getlblTableLengthX() && y >= 0 && y < getTablePanel().getlblTableLengthY())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Getter pt. tablePanel
	 * @return mainFrame.getGamePanel().getTablePanel()
	 */
	private TablePanel getTablePanel()
	{
		return mainFrame.getGamePanel().getTablePanel();
	}
	
}