import java.util.ArrayList;

public class FlagClass {
	private ArrayList<Integer> flagX = new ArrayList<Integer>();
	private ArrayList<Integer> flagY = new ArrayList<Integer>();
	
	/**
	 * Metoda adauga un nou steag in lista.
	 * @param x Coordonata x a steagului (linia).
	 * @param y Coordonata y a steagului (coloana).
	 */
	public void addFlag(int x, int y){
		flagX.add(x);
		flagY.add(y);
	}
	
	/**
	 * Metoda verifica daca pe pozitia x, y exista steag.
	 * @param x Coordonata x a steagului (linia).
	 * @param y Coordonata y a steagului (coloana).
	 * @return True daca exista steag, false daca nu exista steag.
	 */
	public boolean isFlag(int x, int y){
		for(int i=0; i<flagX.size(); ++i){
			if(flagX.get(i) == x && flagY.get(i) == y) return true;
		}
		return false;
	}
	
	/**
	 * Metoda sterge steagul din lista.
	 * @param x Coordonata x a steagului (linia).
	 * @param y Coordonata y a steagului (coloana).
	 */
	public void removeFlag(int x, int y){
		for(int i=0; i<flagX.size(); ++i){
			if(flagX.get(i) == x && flagY.get(i) == y){
				flagX.remove(i);
				flagY.remove(i);
			}
		}
	}
}
