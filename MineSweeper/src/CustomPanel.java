import javax.swing.JPanel;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JTextField;


@SuppressWarnings("serial")
public class CustomPanel extends JPanel {
	private JTextField tfHeight;
	private JTextField tfWidth;
	private JTextField tfMines;

	/**
	 * Constructor pt. Clasa CustomPanel
	 */
	public CustomPanel() {
		setLayout(new GridLayout(3, 2));
		
		JLabel lblHeight = new JLabel("Height: ");
		add(lblHeight);
		
		tfHeight = new JTextField();
		add(tfHeight);
		tfHeight.setColumns(10);
		
		JLabel lblWidth = new JLabel("Width: ");
		add(lblWidth);
		
		tfWidth = new JTextField();
		add(tfWidth);
		tfWidth.setColumns(10);
		
		JLabel lblMines = new JLabel("Mines: ");
		add(lblMines);
		
		tfMines = new JTextField();
		add(tfMines);
		tfMines.setColumns(10);

	}

	/**
	 * @return the tfHeight
	 */
	public JTextField getTfHeight() {
		return tfHeight;
	}

	/**
	 * @return the tfWidth
	 */
	public JTextField getTfWidth() {
		return tfWidth;
	}

	/**
	 * @return the tfMines
	 */
	public JTextField getTfMines() {
		return tfMines;
	}

}
