import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JTextField;



public class ChronometerClass 
{
	private Timer timer;
	private int seconds;
	private int maxValue;
	private int period;
	private JTextField tfAux;
	private boolean enable = true;
	private String format;
    
	/**
	 * Constructor pt. clasa ChronometerClass.
	 * @param perio Perioada (sec).
	 * @param maxValue Valoarea maxima la care sa se opreasca taimarul.
	 * @param tfAux TextField`ul in care este afisat timpul.
	 */
    public ChronometerClass(int period, int maxValue, JTextField tfAux) 
    {
        timer = new Timer();
           
        this.period = period;
        this.seconds = 0;
        this.maxValue = maxValue;
        this.tfAux = tfAux;
	}
    
    /**
     * Metoda returneaza daca taimarul este pornit sa oprit.
     * @return True - pornit, false - oprit.
     */
    public boolean isEnable()
    {
    	return enable;
    }
    
    /**
     * Metoda porneste taimarul.
     */
    public void start()
    {
    	enable = false;
    	timer.schedule(new RemindTask(), 0, period*1000);
    }
    
    /**
     * Metoda opreste taimarul.
     */
    public void stop()
    {
    	enable = true;
    	timer.cancel();
    }
    
    class RemindTask extends TimerTask 
    {
    	
    	/**
    	 * Metoda executal codul de numarare si afisare a timpului (periodic).
    	 */
    	public void run() 
    	{
            if (seconds < maxValue) 
            {
                ++seconds;
                if(seconds < 10)
                {
                	format = "00" + seconds;
                	tfAux.setText(format);
                }else if(seconds < 100)
                {
                	format = "0" + seconds;
                	tfAux.setText(format);
                }else 
                {
                	format = "" + seconds;
                	tfAux.setText(format);
                }
                UtilClass.TIME = format;
            } 
            else 
            {
                timer.cancel();  
            }
    	}
    }
}
