import java.awt.Color;

public class Constants
{
	public static final int EMPTY_FIELD = 0;
	public static final int MINE = -1;
	public static final int NOT_ENABLE = -2;
	public static final int []X = {0, 1, 1, 1, 0, -1, -1, -1};
	public static final int []Y = {-1, -1, 0, 1, 1, 1, 0, -1};
	public static final Color []Colors = {Color.WHITE,Color.BLUE, Color.GREEN, Color.RED, Color.PINK, Color.MAGENTA,
										  Color.CYAN, Color.BLACK, Color.GRAY};
	public static final String FLAG_PIC = "C:\\Users\\Dan\\Desktop\\Proiect POO\\Icon\\Flag.jpg";
	public static final String OMG_PIC = "C:\\Users\\Dan\\Desktop\\Proiect POO\\Icon\\omg.jpg";
	public static final String MINE_PIC = "C:\\Users\\Dan\\Desktop\\Proiect POO\\Icon\\Mine.jpg";
	public static final String NOT_MINE_PIC = "C:\\Users\\Dan\\Desktop\\Proiect POO\\Icon\\NotMine.jpg";
	public static final String RED_MINE_PIC = "C:\\Users\\Dan\\Desktop\\Proiect POO\\Icon\\RedMine.jpg";
	public static final String SAD_PIC = "C:\\Users\\Dan\\Desktop\\Proiect POO\\Icon\\sad.jpg";
	public static final String SMILE_PIC = "C:\\Users\\Dan\\Desktop\\Proiect POO\\Icon\\smile.jpg";
	public static final String WIN_PIC = "C:\\Users\\Dan\\Desktop\\Proiect POO\\Icon\\win.jpg";
	public static final String ABOUT =
			"-->> Autor: Butiri Alexandru Dan\n" +
			"-->> Grupa: 30224 (An 2, CTIro)\n" +
			"-->> Materie: POO - Programare Orientata pe Obiecte\n" +
			"-->> Profesor: Dr. Anca Ciurte\n" +
			"-->> Scop: MineSweeper";
	public static final String RULES = 
			"Obiectivul\n\n" +
			"Gasiti patratele goale si evitati minele. Cu cat eliberati mai rapid tabla, cu atat obtineti un scor mai mare.\n\n" +
			"Tabla\n\n" +
			"Minesweeper are trei table standard din care sa alegeti, fiecare din ce in ce mai dificila:\n" +
			"->Incepator: 81 de placute, 10 de mine\n" +
			"->Intermediar: 256 de placute, 40 de mine\n" +
			"->Expert: 480 de placute, 99 de mine\n" +
			"De asemenea, aveti posibilitatea sa creati o tabla particularizata, facand clic pe meniul Joc, apoi pe Optiuni.\n" +
			"Minesweeper accepta placi de pana la 720 de placute si 668 de mine.\n\n" +
			"Cum se joaca\n\n" +
			"Regulile din Minesweeper sunt simple:\n" +
			"->Descoperiti o mina si jocul se incheie.\n" +
			"->Descoperiti un patrat simplu si continuati sa jucati.\n" +
			"->Descoperiti un numar si acesta va indica numarul de mine care stau ascunse in cele opt patrate inconjuratoare -\n" +
			"informatii pe care le utilizati pentru a deduce pe care patrate din apropiere este sigur sa faceti clic.\n\n" +
			"Indicii si sfaturi\n\n" +
			"=>Marcati minele: Daca suspectati ca un patrat ascunde o mina, faceti clic cu butonul din dreapta pe el.\n" +
			"Astfel se va adauga un steag la patrat. (Daca nu sunteti sigur, faceti din nou clic cu butonul din dreapta pe patrat\n" +
			"pentru a adauga un semn de intrebare.)\n\n" +
			"=>Studiati modelele: Daca trei patrate dintr-un rand afiseaza 2-3-2, atunci stiti ca trei mine sunt aliniate\n" +
			"probabil sub acel rand. Daca pe un patrat este afisata culoarea 8, toate patratele din jur sunt mine.\n\n" +
			"=>Explorati necunoscutul: Nu sunteti sigur unde sa faceti clic in continuare? incercati sa dezvaluiti\n" +
			"un teritoriu neexplorat. Este de preferat sa faceti clic in mijlocul unei zone de patratele nemarcate decat intr-o\n" +
			"zona unde suspectati ca se afla mine.";
}
